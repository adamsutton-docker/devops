---

variables:
    DOCKER_REGISTRY: "$CI_REGISTRY"
    DOCKER_REGISTRY_AUTH_TOKEN: ""
    DOCKER_REGISTRY_USER: "$CI_REGISTRY_USER"
    DOCKER_REGISTRY_PASS: "$CI_REGISTRY_PASSWORD"

    DOCKER_FILE: "Dockerfile"
    DOCKER_CONTEXT: "context"
    DOCKER_IMAGE_PATH: "$CI_REGISTRY_IMAGE"
    DOCKER_IMAGE_NAME: ""
    DOCKER_IMAGE_TAGS: "$CI_COMMIT_SHORT_SHA"
    DOCKER_IMAGE_TAG_WIP: "$CI_COMMIT_SHORT_SHA.WIP"

#
# Setup the docker authentication/configuration, based on either a token
# or username/password
#

.docker-auth:
    before_script:
        - |
            if [ -z "${DOCKER_REGISTRY_AUTH_TOKEN}" ]; then
                DOCKER_REGISTRY_AUTH_TOKEN=$(echo -n "${DOCKER_REGISTRY_USER}:${DOCKER_REGISTRY_PASS}" | base64 | tr -d "\n");
            fi
            CNFDIR=~/.docker
            if [ -d /kaniko ]; then
                CNFDIR=/kaniko/.docker
            fi
            mkdir -p ${CNFDIR}
            cat > ${CNFDIR}/config.json <<EOF
            {
                "auths": {
                    "${DOCKER_REGISTRY}": {
                        "auth": "${DOCKER_REGISTRY_AUTH_TOKEN}"
                    }
                },
                "registry-mirrors": [
                    "https://dockerhub-proxy.uk.cambridgeconsultants.com"
                ]
            }
            EOF

#
# Build a temporary docker container
#
# Note: this generates a temporary label postfixed with .WIP which
#       is removed post validation
#
# Note: keeping both the kaniko and dind build options in here as templates
#       kaniko is generally faster, although not always by much and I found
#       this particular build results in a smaller size in the registry when
#       built using dind
#

.docker-build-kaniko:
    extends: .docker-auth
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [ "" ]
    script:
        - >-
            /kaniko/executor
            -c "${DOCKER_CONTEXT}"
            -f "${DOCKER_FILE}"
            -d "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG_WIP}"

.docker-build-dind:
    extends: .docker-auth
    image: docker:latest
    services:
        - name: docker:dind
    script:
        - docker build
            -t "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG_WIP}"
            -f "${DOCKER_FILE}"
            "${DOCKER_CONTEXT}"
        - docker push "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG_WIP}"

#
# Verify a docker container
#
# The script can be overridden to be anything that usefully validates
# that the generated container is working
#

.docker-verify:
    image: "$DOCKER_IMAGE_PATH$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG_WIP"
    script:
        - make -C verify

#
# Publish the container (removing WIP tag)
#

.docker-publish:
    extends: .docker-auth
    image: docker:latest
    variables:
        GIT_STRATEGY: none
    script: |
        for tag in ${DOCKER_IMAGE_TAGS}; do
            docker manifest create "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${tag}" \
                "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG_WIP}"
            docker manifest push "${DOCKER_IMAGE_PATH}${DOCKER_IMAGE_NAME}:${tag}"
        done
