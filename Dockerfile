#
# Stage 1 - build required python packages, avoid increasing the size of
#           the final container or needing to have containers with single
#           monolithic RUN stage
#

FROM alpine:3.15 AS builder

# Add the build tools
RUN set -xe ; \
    apk add --update --no-cache \
        alpine-sdk \
        libffi-dev \
        python3-dev \
    ;

# Install python3
RUN set -xe ; \
    apk add --update --no-cache \
        python3 \
        py3-pip \
        ; \
    pip install -U pip

# Install ansible packages
RUN set -xe ; \
    pip install --prefix /tmp/site-packages \
        ansible-core==2.12.4 \
        ansible==5.6.0 \
        boto3 \
        dnspython \
        ;

# Install helper packages
RUN set -xe ; \
    pip install --prefix /tmp/site-packages \
        awscli \
        ;

#
# Stage 2 - main container, add basic tools and copy py packages from builder
#

FROM alpine:3.15

# Basic packages
RUN set -xe ; \
    apk add --update --no-cache \
        curl \
        git \
        make \
        vim \
        ;

# Python3
RUN set -xe ; \
    apk add --update --no-cache \
        python3 \
        py3-pip \
        ; \
    pip install -U pip ;

# SSH client
RUN set -xe ; \
    apk add --update --no-cache \
        openssh-client \
        sshpass \
        ;

# Python packages from builder
COPY --from=builder /tmp/site-packages /usr/local

# Update PYTHONPATH
ENV PYTHONPATH "${PYTHONPATH}:/usr/local/lib/python3.9/site-packages"
